# Contributing

## Bug reports
I always appreciate bug reports, although I do not appreciate bugs themselves.
Before reporting a bug, please make sure someone else hasn't already done so by
checking the [issue tracker](https://gitlab.com/AGausmann/samurai/issues).

## Merge requests
I don't accept just any merge request. If there is a related issue which has
gained popular support, or I otherwise deem it to be a reasonable contribution,
I will merge it.

Also, I have the CI programmed to automatically build merge requests and post
the status of them back when it is done. I have a policy of _no false
negatives_ and _some false positives._ If the CI fails to build and
test the merge request, I will _not_ merge it until it is fixed.

### Content
Merge requests should have a productive effect on the _behavior_ of the code.
If you wish to provide a bugfix, please do! New features are also acceptable;
just make sure that there is enough support from the community to accept the
change. 

In contrast to behavior, I will not accept merge requests that
primarily contain changes to the _appearance_ of the code. If it's something
that an IDE did automatically, like removing unused imports, I may allow it,
but reformatting an entire file is not okay.

### Code style
Although I am OCD about my own code style, I try to be lenient about the styles
that others use. My only request is that if you change a section of lines in an
existing file, the style used should be _comparable_ to the original. In any
case, make sure that the style used is _easily readable._
