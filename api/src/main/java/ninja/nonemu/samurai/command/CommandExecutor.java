package ninja.nonemu.samurai.command;

/**
 * Handles a command when it is executed by a user or console.
 */
public interface CommandExecutor {

    /**
     * Called by the command system when the command is executed.
     * @param sender The sender of the command.
     * @param info Information about the command. Useful if one executor handles multiple commands (ie. plugins).
     * @param label The command label sent by the user; may be different if an alias is used.
     * @param args Additional arguments sent by the user.
     * @return true if the command was handled successfully; false otherwise.
     * @throws Exception if an error occurs during command handling.
     */
    boolean onCommand(CommandSender sender, CommandInfo info, String label, String[] args) throws Exception;
}

