package ninja.nonemu.samurai.command;

/**
 * Defines the properties of a command, which is used during registration and passed to executors.
 */
public class CommandInfo {
    private final String name;
    private final String description;
    private final String usage;
    private final boolean isPrivileged;

    /**
     * Constructs a new instance.
     * @param name The name of the command (what the user sends to the bot).
     * @param description A description of what the command does.
     * @param usage An example of how to use the bot. Backus-Naur Form (BNF) is recommended.
     * @param isPrivileged Whether usage of the command should be restricted to operators (true) or public (false).
     */
    public CommandInfo(String name, String description, String usage, boolean isPrivileged) {
        this.name = name;
        this.description = description;
        this.usage = usage;
        this.isPrivileged = isPrivileged;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getUsage() {
        return usage;
    }

    public boolean isPrivileged() {
        return isPrivileged;
    }
}
