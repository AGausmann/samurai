package ninja.nonemu.samurai.command;

import ninja.nonemu.irc.ChatRecipient;

/**
 * Defines an entity (usually a user or console) that is able to execute a command.
 */
public interface CommandSender extends ChatRecipient {
    /**
     * Determines whether the command sender is allowed to run privileged commands.
     * @return true if the sender is an operator; false otherwise.
     */
    boolean isOperator();

    /**
     * Determines whether the command sender is allowed to run commands when the bot's whitelist is enabled.
     * @return true if the sender is whitelisted; false otherwise.
     */
    boolean isWhitelisted();

    /**
     * Determines whether the command sender is banned from using commands.
     * @return true if the sender is blacklisted; false otherwise.
     */
    boolean isBlacklisted();
}
