package ninja.nonemu.samurai.command;

import ninja.nonemu.samurai.connection.UserMask;

/**
 * Defines a command system that registers commands and sends them to executors.
 * @see CommandInfo
 * @see CommandExecutor
 */
public interface CommandSystem {

    String getCommandPrefix();

    /**
     * Registers a command with the system so users can execute it.
     * @param info The information about the registered command, including permissions and usage descriptions.
     * @param executor The instance responsible for executing the command.
     */
    void registerCommand(CommandInfo info, CommandExecutor executor);

    /**
     * Gets a list of commands that have been registered.
     */
    CommandInfo[] getCommands();

    /**
     * Gets information about a registered command, referencing it by name.
     *
     * @param name The name of the command.
     */
    CommandInfo getCommand(String name);

    void executeCommand(CommandSender sender, String label, String[] args);

    /**
     * Determines whether the command sender is allowed to run privileged commands.
     *
     * @param mask The mask of the sender to check.
     * @return true if the sender is an operator; false otherwise.
     */
    boolean isOperator(UserMask mask);

    /**
     * Adds a sender to the operator list. Any sender with that mask can run privileged commands.
     * @param mask The mask to add.
     */
    void addOperator(UserMask mask);

    /**
     * Removes a sender from the operator list. If they previously had privileges, they won't anymore.
     * @param mask The mask to remove.
     */
    void removeOperator(UserMask mask);

    /**
     * Determines whether the command sender is allowed to run commands when the bot's whitelist is enabled.
     *
     * @param mask The mask of the sender to check.
     * @return true if the sender is whitelisted; false otherwise.
     */
    boolean isWhitelisted(UserMask mask);

    /**
     * Adds a sender to the whitelist. Any sender with that mask can run commands when whitelist is enabled.
     * @param mask The mask to add.
     */
    void addWhitelisted(UserMask mask);

    /**
     * Removes a sender from the whitelist. If they previously had privileges, they won't anymore.
     * @param mask The mask to remove.
     */
    void removeWhitelisted(UserMask mask);

    boolean isWhitelistEnabled();

    void setWhitelistEnabled(boolean enabled);

    /**
     * Determines whether the command sender is banned from using commands.
     *
     * @param mask The mask of the sender to check.
     * @return true if the sender is blacklisted; false otherwise.
     */
    boolean isBlacklisted(UserMask mask);

    /**
     * Adds a sender to the blacklist. Any sender with that mask cannot run commands at all.
     * @param mask The mask to add.
     */
    void addBlacklisted(UserMask mask);

    /**
     * Removes a sender from the blacklist. If they previously didn't have privileges, they will now.
     * @param mask The mask to remove.
     */
    void removeBlacklisted(UserMask mask);
}
