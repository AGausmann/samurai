package ninja.nonemu.samurai.connection;

public class UserMask implements Comparable<UserMask> {
    private final String mask;

    public UserMask(String mask) {
        this.mask = mask;
    }

    public UserMask(String nickname, String username, String hostname) {
        this(nickname + "!" + username + "@" + hostname);
    }

    public String getNickname() {
        if (mask.contains("!")) {
            return mask.substring(0, mask.indexOf("!"));
        }
        return mask;
    }

    public String getUsername() {
        if (mask.contains("!")) {
            if (mask.contains("@")) {
                return mask.substring(mask.indexOf("!") + 1, mask.indexOf("@"));
            }
            return mask.substring(mask.indexOf("!") + 1);
        }
        return null;
    }

    public String getHostname() {
        if (mask.contains("@")) {
            return mask.substring(mask.indexOf("@") + 1);
        }
        return null;
    }

    public boolean isWildcard() {
        return mask.contains("*") || mask.contains("?");
    }

    public String getMask() {
        return mask;
    }

    public boolean matches(UserMask mask) {
        return matches(getNickname(), mask.getNickname()) && matches(getUsername(), mask.getUsername())
                && matches(getHostname(), mask.getHostname());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserMask)) return false;

        UserMask userMask = (UserMask) o;

        return mask.equalsIgnoreCase(userMask.mask);

    }

    @Override
    public int hashCode() {
        return mask.toLowerCase().hashCode();
    }

    private static boolean matches(String a, String b) {
        if (a == null || b == null) {
            return true;
        }

        a = a.toLowerCase();
        b = b.toLowerCase();

        if (a.equals(b)) {
            return true;
        }

        String pA = "^" + a.replace(".", "\\.").replace("*", ".*").replace("?", ".") + "$";
        String pB = "^" + b.replace(".", "\\.").replace("*", ".*").replace("?", ".") + "$";

        return a.matches(pB) || b.matches(pA);
    }

    @Override
    public int compareTo(UserMask o) {
        return String.CASE_INSENSITIVE_ORDER.compare(mask, o.mask);
    }

    @Override
    public String toString() {
        return mask;
    }
}
