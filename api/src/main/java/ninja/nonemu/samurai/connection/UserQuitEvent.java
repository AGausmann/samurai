package ninja.nonemu.samurai.connection;

import ninja.nonemu.samurai.event.Event;

public class UserQuitEvent extends Event {
    private final User user;
    private final String comment;

    public UserQuitEvent(User user, String comment) {
        this.user = user;
        this.comment = comment;
    }

    public User getUser() {
        return user;
    }

    public String getComment() {
        return comment;
    }
}
