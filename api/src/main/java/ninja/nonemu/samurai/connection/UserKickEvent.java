package ninja.nonemu.samurai.connection;

public class UserKickEvent extends UserLeaveEvent {
    private final User sender;

    public UserKickEvent(User sender, User user, Channel channel, String comment) {
        super(user, channel, comment);

        this.sender = sender;
    }

    public User getSender() {
        return sender;
    }
}
