package ninja.nonemu.samurai.connection;

import ninja.nonemu.samurai.event.Event;

public class UserLeaveEvent extends Event {
    private final User user;
    private final Channel channel;
    private final String comment;

    public UserLeaveEvent(User user, Channel channel, String comment) {
        this.user = user;
        this.channel = channel;
        this.comment = comment;
    }

    public User getUser() {
        return user;
    }

    public Channel getChannel() {
        return channel;
    }

    public String getComment() {
        return comment;
    }
}
