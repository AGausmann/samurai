package ninja.nonemu.samurai.connection;

public class BotKickEvent extends BotLeaveEvent {
    private final User sender;

    public BotKickEvent(User sender, Channel channel, String comment) {
        super(channel, comment);
        this.sender = sender;
    }

    public User getSender() {
        return sender;
    }
}
