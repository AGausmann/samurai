package ninja.nonemu.samurai.connection;

import ninja.nonemu.irc.ChatRecipient;
import ninja.nonemu.irc.ChatSender;

public class NoticeEvent extends ChatEvent{
    public NoticeEvent(ChatSender sender, ChatRecipient target, String text) {
        super(sender, target, text);
    }
}
