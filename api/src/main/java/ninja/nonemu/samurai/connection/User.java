package ninja.nonemu.samurai.connection;

import ninja.nonemu.irc.ChatRecipient;
import ninja.nonemu.irc.ChatSender;
import ninja.nonemu.samurai.command.CommandSender;

public interface User extends ChatSender, ChatRecipient, CommandSender {

    default String getName() {
        return getNickname();
    }

    UserMask getMask();

    String getNickname();

    String getUsername();

    String getHostname();

    Connection getConnection();
}
