package ninja.nonemu.samurai.connection;

import ninja.nonemu.samurai.event.Event;

/**
 * Dispatched by the connection manager when the bot disconnects from a server.
 */
public class BotDisconnectEvent extends Event {
    private final Connection connection;
    private final String comment;

    public BotDisconnectEvent(Connection connection, String comment) {
        this.connection = connection;
        this.comment = comment;
    }

    public Connection getConnection() {
        return connection;
    }

    public String getComment() {
        return comment;
    }
}
