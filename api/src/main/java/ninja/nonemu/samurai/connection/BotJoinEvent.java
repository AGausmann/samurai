package ninja.nonemu.samurai.connection;

import ninja.nonemu.samurai.event.Event;

public class BotJoinEvent extends Event {
    private final Channel channel;

    public BotJoinEvent(Channel channel) {
        this.channel = channel;
    }

    public Channel getChannel() {
        return channel;
    }
}
