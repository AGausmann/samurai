package ninja.nonemu.samurai.connection;

import java.io.IOException;
import ninja.nonemu.irc.Message;

public interface Connection {

    /**
     * Gets the identifier string of this connection as set by {@link ConnectionBuilder#id(String)}.
     * @return The ID string (case insensitive).
     */
    String getId();

    /**
     * Sends a custom message to the server.
     * @param message The message to send.
     */
    void sendMessage(Message message);

    /**
     * Checks whether a connection exists by checking if there is a socket and it says it is connected.
     * @return true if there is a connection; false otherwise.
     */
    boolean isConnected();

    /**
     * Connects to the network using the details provided during construction.
     *
     * <br>This may send any/all of the following: PASS, NICK, and USER messages.
     *
     * @throws IOException if an IO error occurs while connecting.
     */
    void connect() throws IOException;

    /**
     * Disconnects from any connected network.
     *
     * @param comment The comment to leave when disconnecting.
     * @throws IOException if an IO error occurs while disconnecting.
     */
    void disconnect(String comment) throws IOException;

    /**
     * Disconnects from any connected network without providing a comment.
     *
     * @throws IOException if an IO error occurs while disconnecting.
     */
    void disconnect() throws IOException;

    /**
     * Attempts to set a new nickname for the client.
     *
     * @param nickname The new nickname.
     */
    void setNickname(String nickname);

    /**
     * Attempts to join a channel.
     *
     * @param channel The channel to join.
     */
    void joinChannel(String channel);

    /**
     * Attempts to leave a channel, leaving a comment behind.
     * @param channel The channel to leave.
     * @param comment The comment/reason for leaving.
     */
    void leaveChannel(String channel, String comment);

    /**
     * Attempts to leave a channel with no comment.
     * @param channel The channel to leave.
     */
    void leaveChannel(String channel);

    /**
     * Sends a chat message to a channel or user connected to the network.
     * @param target The entity receiving the message.
     * @param message The message to send.
     */
    void sendChat(String target, String message);

    Channel getChannel(String channel);

    User getUser(String userId);

    User getUser(UserMask userMask);

    /**
     * Gets the currently-configured host that the client is connecting to.
     */
    String getHost();

    /**
     * Gets the currently-configured port that the client is connecting to.
     */
    int getPort();

    /**
     * Gets the currently-configured nickname of the client.
     */
    String getNickname();
}
