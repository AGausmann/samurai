package ninja.nonemu.samurai.connection;

public class UserKillEvent extends UserQuitEvent {
    private final User sender;

    public UserKillEvent(User sender, User user, String comment) {
        super(user, comment);

        this.sender = sender;
    }

    public User getSender() {
        return sender;
    }
}
