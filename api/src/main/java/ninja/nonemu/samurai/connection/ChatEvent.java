package ninja.nonemu.samurai.connection;

import ninja.nonemu.irc.ChatRecipient;
import ninja.nonemu.irc.ChatSender;
import ninja.nonemu.samurai.event.Event;

/**
 * THis event is dispatched by the connection manager when a player sends a chat message that is read by the bot.
 */
public class ChatEvent extends Event {
    private final ChatSender sender;
    private final ChatRecipient target;
    private final String text;

    public ChatEvent(ChatSender sender, ChatRecipient target, String text) {
        this.sender = sender;
        this.target = target;
        this.text = text;
    }

    public ChatSender getSender() {
        return sender;
    }

    public ChatRecipient getTarget() {
        return target;
    }

    public String getText() {
        return text;
    }
}
