package ninja.nonemu.samurai.connection;

import ninja.nonemu.samurai.event.Event;

public class UserJoinEvent extends Event {
    private final User user;
    private final Channel channel;

    public UserJoinEvent(User user, Channel channel) {
        this.user = user;
        this.channel = channel;
    }

    public User getUser() {
        return user;
    }

    public Channel getChannel() {
        return channel;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }
}
