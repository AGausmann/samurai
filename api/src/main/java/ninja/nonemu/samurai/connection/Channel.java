package ninja.nonemu.samurai.connection;

import ninja.nonemu.irc.ChatRecipient;

public interface Channel extends ChatRecipient {
    String getName();

    Connection getConnection();
}
