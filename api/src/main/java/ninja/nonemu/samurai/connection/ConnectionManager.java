package ninja.nonemu.samurai.connection;

/**
 * Manages connections to IRC networks.
 * Since multiple simultaneous connections are allowed, this class is provided to allow plugins to add and remove
 * connections on their own.
 *
 * @see Connection
 */
public interface ConnectionManager {

    /**
     * Gets a list of all the connections that are currently registered. They may or may not be connected.
     */
    Connection[] getConnections();

    /**
     * Gets a connection given its ID string.
     * @param id The ID of the connection that was given when it was built. (case insensitive).
     * @return The connection found, or null if there is no connection with that ID.
     */
    Connection getConnection(String id);

    /**
     * Creates a builder that will be used to create the new connection.
     * When {@link ConnectionBuilder#build()} is called, the newly-created connection will automatically be added.
     * @return The new connection builder.
     */
    ConnectionBuilder addConnection();

    /**
     * Removes a connection if it has been added.
     * @param connection The connection to remove.
     */
    default void removeConnection(Connection connection) {
        removeConnection(connection.getId());
    }

    /**
     * Removes a connection if it has been added.
     * @param id The ID string of the connection to remove (case insensitive).
     */
    void removeConnection(String id);
}
