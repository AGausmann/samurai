package ninja.nonemu.samurai.connection;

/**
 * The class which builds new {@link Connection} instances.
 * An implementation is constructed and returned when you call {@link ConnectionManager#addConnection()}. That
 * implementation will automatically add the connection when you call {@link #build()}.
 *
 * @see ConnectionManager
 */
public interface ConnectionBuilder {

    /**
     * Sets the identifying string of the connection.
     * This is used by methods like {@link ConnectionManager#getConnection(String)} and
     * {@link ConnectionManager#removeConnection(String)}. It will also be returned by {@link Connection#getId()}.
     * If no ID is specified, the hostname is used as the ID.
     *
     * @param id The new connection's ID (case insensitive).
     */
    ConnectionBuilder id(String id);

    /**
     * Sets the hostname of the server to connect to. This is required; no default is available.
     *
     * @param host The hostname (case insensitive).
     */
    ConnectionBuilder host(String host);

    /**
     * Sets the port of the server to connect to (default 6667).
     *
     * @param port The port.
     */
    ConnectionBuilder port(int port);

    /**
     * Sets the nickname that the bot will associate as. This is required; no default is available.
     *
     * @param nickname The nickname (case insensitive).
     */
    ConnectionBuilder nickname(String nickname);

    /**
     * Sets the channels that the bot will autoconnect to. If null or empty, no join will be attempted. Default is null.
     *
     * @param channels A comma-separated list of channels to join, or null.
     */
    ConnectionBuilder channels(String channels);

    /**
     * Sets the channels that the bot will autoconnect to. If null or empty, no join will be attempted. Default is null.
     *
     * @param channels An array of channels to join, or null.
     */
    default ConnectionBuilder channels(String[] channels) {
        return channels(channels == null ? null : String.join(",", (CharSequence[]) channels));
    }

    /**
     * Sets the password that will be used to access the server. If null or empty, no password will be sent. Default is null.
     *
     * @param password The password.
     */
    ConnectionBuilder password(String password);

    /**
     * Sets whether the connection should attempt to reconnect when disconnection is detected. Default is true.
     *
     * @param autoconnect Whether or not to autoconnect.
     */
    ConnectionBuilder autoconnect(boolean autoconnect);

    /**
     * Sets how long to wait before trying to reconnect, if autoconnect is enabled. Default is 5 seconds.
     *
     * @param cooldown How long the cooldown period is in milliseconds.
     */
    ConnectionBuilder autoconnectCooldown(long cooldown);

    /**
     * Sets how many times to try reconnecting before giving up. If zero or negative, there is no limit. Default is zero.
     *
     * @param limit The number of retries allowed.
     */
    ConnectionBuilder autoconnectLimit(int limit);

    /**
     * Validates the arguments, builds the Connection instance and returns it.
     * If this is an instance returned by {@link ConnectionManager#addConnection()}, the connection will be added to
     * the bot's connection manager. If autoconnect is enabled and the above is true, connection will automatically
     * be attempted, otherwise it will not.
     *
     * @return The built connection.
     */
    Connection build();
}
