package ninja.nonemu.samurai.connection;

import ninja.nonemu.samurai.event.Event;

/**
 * Dispatched by the connection manager when the bot has successfully connected to a server.
 */
public class BotConnectEvent extends Event {
    private final Connection connection;

    public BotConnectEvent(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }
}
