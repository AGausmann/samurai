package ninja.nonemu.samurai.connection;

import ninja.nonemu.samurai.event.Event;

public class BotLeaveEvent extends Event {
    private final Channel channel;
    private final String comment;

    public BotLeaveEvent(Channel channel, String comment) {
        this.channel = channel;
        this.comment = comment;
    }

    public Channel getChannel() {
        return channel;
    }

    public String getComment() {
        return comment;
    }
}
