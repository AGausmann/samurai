package ninja.nonemu.samurai.event;

/**
 * There are six priority options depending on how early a handler should handle events compared to other handlers.
 * Event handlers with higher priority get to handle (and possibly cancel) the event. The Monitor priority, which is
 * the lowest priority option, should only be used to read the state of the event. No changes, including cancellation,
 * should be made to the event once it has reached that point. Medium is the default priority.
 */
public enum Priority {
    HIGHEST,
    HIGH,
    MEDIUM,
    LOW,
    LOWEST,
    MONITOR
}
