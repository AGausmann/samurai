package ninja.nonemu.samurai.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A method annotated with this can be registered as an event handler.
 * Event handler methods _must_ take only 1 operand which is either {@link Event} or one of its subclasses.
 * Any dispatched events which are subclasses of the operand type, either directly or transitively, will be passed to
 * this event handler once it has been registered.
 *
 * <br>This event system architecture is heavily based upon the Bukkit API.
 * @see Priority
 * @see Event
 * @see EventSystem#registerHandlers(Object)
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventHandler {
    /**
     * Determines what priority the handler gets.
     * The higher priority is, the sooner the handler will be able to receive (and cancel before other handlers receive)
     * an event.
     */
    Priority priority() default Priority.MEDIUM;

    /**
     * Determines whether the handler wants to handle events that have already been cancelled.
     * Defaults to false, which means the handler will be skipped if an event is cancelled.
     */
    boolean acceptCancelled() default false;

    /**
     * Determines whether the handler wants to handle events which are subclasses of the event for which the handler
     * was registered.
     * Defaults to true, which means the event system will also send subclasses.
     */
    boolean acceptSubclass() default true;
}
