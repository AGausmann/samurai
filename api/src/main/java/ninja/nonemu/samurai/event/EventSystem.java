package ninja.nonemu.samurai.event;

/**
 * The event system handles how {@link Event}s are passed around throughout the bot.
 * Plugins may register {@link EventHandler}s that handle when a certain event has been dispatched.
 */
public interface EventSystem {

    /**
     * Registers handlers which have been defined as annotated methods in an object.
     * This method will throw an exception if any of the annotated methods are of the wrong format; see the
     * documentation at {@link EventHandler} for more information about event handler formats.
     * @param o
     */
    void registerHandlers(Object o);

    /**
     * Dispatches an event to any appropriate handlers.
     * @param event The event to dispatch.
     */
    void dispatchEvent(Event event);
}
