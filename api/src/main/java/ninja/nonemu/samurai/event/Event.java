package ninja.nonemu.samurai.event;

/**
 * This is the base class for an event, which is pretty boring.
 * There won't be much to see here because subclasses of Event define their own properties depending upon the context
 * of the event. The one global interface for events is the cancellation system, an indicator from higher-priority
 * event handlers that the event should no longer be processed.
 */
public class Event {
    private boolean cancelled;

    public Event() {
        cancelled = false;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void cancel() {
        cancelled = true;
    }
}
