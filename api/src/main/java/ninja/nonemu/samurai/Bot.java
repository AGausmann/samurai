package ninja.nonemu.samurai;

import ninja.nonemu.samurai.command.CommandSystem;
import ninja.nonemu.samurai.connection.ConnectionManager;
import ninja.nonemu.samurai.event.EventSystem;
import ninja.nonemu.samurai.plugin.PluginManager;
import org.apache.logging.log4j.Logger;

/**
 * Describes the core functionality of a samurai bot.
 */
public interface Bot {

    Logger getLogger();

    EventSystem getEventSystem();

    ConnectionManager getConnectionManager();

    CommandSystem getCommandSystem();

    PluginManager getPluginManager();

    String getProperty(String name);

    void quit(int status);

    default void quit() {
        quit(0);
    }

    String getVersion();
}
