package ninja.nonemu.samurai.plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import ninja.nonemu.samurai.Bot;
import ninja.nonemu.samurai.command.CommandExecutor;
import ninja.nonemu.samurai.command.CommandInfo;
import ninja.nonemu.samurai.command.CommandSender;
import org.apache.logging.log4j.Logger;

/**
 * A highly-recommended subclass for plugins that are implemented using the JAR loading mechanism.
 *
 * <br> A base for plugins that provides access to the bot and its subsystems as well as some other cool features.
 */
public abstract class PluginBase implements Plugin, CommandExecutor {
    private static Bot defaultBot;

    public static void setDefaultBot(Bot defaultBot) {
        PluginBase.defaultBot = defaultBot;
    }

    private final Bot bot;
    private final Logger logger;
    private final Properties properties;
    private final String name;
    private final File pluginDirectory;

    protected PluginBase(String name) {
        bot = defaultBot;
        logger = bot.getLogger();
        properties = new Properties();
        this.name = name;
        pluginDirectory = new File("./plugins/" + name);
    }

    /**
     * Loads the properties from the plugin.properties file stored under the plugin's base folder.
     * @throws IOException if an I/O error occurs.
     */
    protected void loadProperties() throws IOException {
        properties.load(new FileInputStream(getFile("plugin.properties")));
    }

    /**
     * Loads the default properties provided by the plugin's JAR.
     * @throws IOException if an I/O error occurs.
     */
    protected void loadDefaultProperties() throws IOException {
        properties.load(getClass().getResourceAsStream("/plugin.properties"));
    }

    /**
     * Stores the properties to the plugin.properties file stored under the plugin's base folder.
     * @throws IOException if an I/O error occurs.
     */
    protected void saveProperties() throws IOException {
        properties.store(new FileOutputStream(getFile("plugin.properties")), name + " Properties");
    }

    /**
     * Gets a property from the loaded properties.
     * @param name The name of the property.
     * @return The value of the property.
     */
    protected String getProperty(String name) {
        return properties.getProperty(name);
    }

    /**
     * Sets a property to the loaded properties.
     * @param name The name of the property.
     * @param value The value of the property.
     */
    protected void setProperty(String name, String value) {
        properties.setProperty(name, value);
    }

    /**
     * Shorthand for registering commands using this plugin as the command executor.
     *
     * <br> Equivalent to {@code getBot().getCommandSystem().registerCommand(info, this)}.
     * @param info The information for the command to register.
     */
    protected void registerCommand(CommandInfo info) {
        bot.getCommandSystem().registerCommand(info, this);
    }

    /**
     * Gets a file from the plugin's base directory.
     *
     * Equivalent to prepending "./plugins/pluginname/" to the filename.
     * @param filename The name of the file.
     * @return The file requested.
     */
    public File getFile(String filename) {
        if (!pluginDirectory.exists()) {
            pluginDirectory.mkdirs();
        }

        return new File("./plugins/" + name + "/" + filename);
    }

    /**
     * Gets the bot that has loaded this plugin.
     */
    public Bot getBot() {
        return bot;
    }

    /**
     * Gets the registered name of the plugin.
     */
    public final String getName() {
        return name;
    }

    /**
     * Gets the logger associated with this plugin.
     */
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public boolean onCommand(CommandSender sender, CommandInfo info, String label, String[] args) throws Exception {
        return false;
    }
}
