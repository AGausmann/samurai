package ninja.nonemu.samurai.plugin;

import java.util.Map;

/**
 * Responsible for loading and managing plugins for the bot.
 */
public interface PluginManager {

    /**
     * Registers a plugin given its name.
     * The bot will handle all of the initialization and management of the plugin.
     * @param plugin The plugin to register.
     * @param name The name of the plugin.
     */
    void registerPlugin(Plugin plugin, String name) throws Exception;

    /**
     * Registers a plugin extended from the plugin base using its given name.
     *
     * <br>Equivalent to {@code registerPlugin(plugin, plugin.getId())}.
     * @param plugin The plugin to register.
     */
    default void registerPlugin(PluginBase plugin) throws Exception {
        registerPlugin(plugin, plugin.getName());
    }

    /**
     * Calls the named plugin's {@link Plugin#cleanup()} method and removes it from the list.
     * @param name The name of the plugin; case-insensitive
     */
    void unregisterPlugin(String name) throws Exception;

    /**
     * Retrieves a registered plugin, given its name.
     * @param name The name of the plugin; case-insensitive
     * @return The plugin with the given name, or {@code null} if no plugin with that name is registered.
     */
    Plugin getPlugin(String name);

    /**
     * Generates a map of all plugins that are registered at the time of this method's execution.
     * <br>The key is a case-insensitive string that denotes the plugin's name, and each corresponding value is that
     * plugin's instance.
     * @return A map containing all registered Plugin instances.
     */
    Map<String, Plugin> getPlugins();
}
