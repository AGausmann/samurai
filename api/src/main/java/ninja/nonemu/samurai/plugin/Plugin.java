package ninja.nonemu.samurai.plugin;

/**
 * Defines an add-on unit that the bot loads. Plugins can register event handlers and commands to interface with the bot.
 *
 * <br>Extending {@link PluginBase} is highly recommended instead of this interface; it provides more utilities for the
 * plugin to use. The bot will not automatically register plugins that do not extend that class, even if they implement
 * this interface.
 */
public interface Plugin {

    /**
     * Called when the bot is ready to initialize the plugin. A good time to register handlers!
     */
    void init() throws Exception;

    /**
     * Called periodically by the plugin manager, once for every iteration of the bot's main loop.
     */
    void run() throws Exception;

    /**
     * Called when the bot is ready to remove the plugin. A good time to free resources!
     */
    void cleanup() throws Exception;
}
