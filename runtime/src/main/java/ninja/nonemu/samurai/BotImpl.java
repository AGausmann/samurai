package ninja.nonemu.samurai;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import ninja.nonemu.samurai.command.CommandSystemImpl;
import ninja.nonemu.samurai.connection.ConnectionManagerImpl;
import ninja.nonemu.samurai.console.ConsoleManager;
import ninja.nonemu.samurai.event.EventSystemImpl;
import ninja.nonemu.samurai.plugin.PluginManager;
import ninja.nonemu.samurai.plugin.PluginManagerImpl;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;

public class BotImpl implements Bot {
    private static final long EXECUTION_INTERVAL_MS = 20;

    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("--debug")) {
                LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
                Configuration config = ctx.getConfiguration();
                LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
                loggerConfig.setLevel(Level.DEBUG);
            }
        }

        BotImpl bot = new BotImpl();
        Logger logger = bot.getLogger();

        bot.init();

        try {
            long lastTime = System.currentTimeMillis();
            while (bot.isRunning()) {
                bot.run();

                long time = System.currentTimeMillis();
                long leftover = EXECUTION_INTERVAL_MS - (time - lastTime);
                if (leftover < 0) {
                    logger.debug("Running {}ms overtime (interval period is {}ms)", -leftover, EXECUTION_INTERVAL_MS);
                } else {
                    try {
                        Thread.sleep(leftover);
                    } catch (InterruptedException e) {
                        logger.error("Sleep interrupted", e);
                    }
                }
                lastTime = time;
            }
        } finally {
            bot.cleanup();
        }

        System.exit(bot.exitCode);
    }

    private final Logger logger;
    private final ConsoleManager consoleManager;
    private final Properties properties;
    private final EventSystemImpl eventSystem;
    private final ConnectionManagerImpl connectionManager;
    private final CommandSystemImpl commandSystem;
    private final PluginManagerImpl pluginManager;
    private String version = null;
    private boolean running;
    private int exitCode;

    public BotImpl() {
        logger = LogManager.getLogger();
        properties = new Properties();
        consoleManager = new ConsoleManager(this);
        eventSystem = new EventSystemImpl(this);
        connectionManager = new ConnectionManagerImpl(this);
        commandSystem = new CommandSystemImpl(this);
        pluginManager = new PluginManagerImpl(this);
    }

    public void init() {
        logger.debug("Initializing bot");

        running = true;

        try {
            loadConfig();
        } catch (IOException e) {
            logger.error("Unable to load bot properties.", e);
            quit(1);
        }

        consoleManager.init();
        eventSystem.init();
        connectionManager.init();
        commandSystem.init();
        pluginManager.init();
    }


    public void run() {
        pluginManager.run();
        commandSystem.run();
        connectionManager.run();
        eventSystem.run();
        consoleManager.run();
    }

    public void cleanup() {
        logger.debug("Cleaning up bot");

        pluginManager.cleanup();
        commandSystem.cleanup();
        connectionManager.cleanup();
        eventSystem.cleanup();
        consoleManager.cleanup();
    }

    private void loadConfig() throws IOException {
        logger.trace("Loading configuration");

        File propertiesFile = new File("./bot.properties");
        if (propertiesFile.exists()) {
            properties.load(new FileInputStream(propertiesFile));
        } else {
            properties.load(getClass().getResourceAsStream("bot.properties"));
            properties.store(new FileOutputStream(propertiesFile), "Samurai bot properties");
        }

        Properties versionProperties = new Properties();
        versionProperties.load(getClass().getResourceAsStream("version.properties"));
        version = versionProperties.getProperty("version");
    }

    private void storeConfig() throws IOException {
        logger.trace("Saving configuration");

        properties.store(new FileOutputStream("./bot.properties"), "Samurai bot properties");
    }

    public boolean isRunning() {
        return running;
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public EventSystemImpl getEventSystem() {
        return eventSystem;
    }

    @Override
    public ConnectionManagerImpl getConnectionManager() {
        return connectionManager;
    }

    @Override
    public CommandSystemImpl getCommandSystem() {
        return commandSystem;
    }

    @Override
    public PluginManager getPluginManager() {
        return pluginManager;
    }

    @Override
    public String getProperty(String name) {
        return properties.getProperty(name);
    }

    public void setProperty(String name, String value) {
        properties.setProperty(name, value);
    }

    @Override
    public void quit(int exitCode) {
        running = false;
        this.exitCode = exitCode;
    }

    @Override
    public String getVersion() {
        return version;
    }
}
