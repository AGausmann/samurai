package ninja.nonemu.samurai.connection;

import ninja.nonemu.samurai.BotImpl;

public class UserImpl implements User {

    private final BotImpl bot;
    private final ConnectionImpl connection;
    private final UserMask mask;

    public UserImpl(BotImpl bot, ConnectionImpl connection, UserMask mask) {
        this.bot = bot;
        this.connection = connection;
        this.mask = mask;
        if (mask.isWildcard()) {
            throw new IllegalArgumentException("Wildcard masks can't be used for users.");
        }
    }

    public UserImpl(BotImpl bot, ConnectionImpl connection, String mask) {
        this(bot, connection, new UserMask(mask));
    }

    /**
     * Determines whether the command sender is allowed to run privileged commands.
     *
     * @return true if the sender is an operator; false otherwise.
     */
    @Override
    public boolean isOperator() {
        return bot.getCommandSystem().isOperator(mask);
    }

    /**
     * Determines whether the command sender is allowed to run commands when the bot's whitelist is enabled.
     *
     * @return true if the sender is whitelisted; false otherwise.
     */
    @Override
    public boolean isWhitelisted() {
        return bot.getCommandSystem().isWhitelisted(mask);
    }

    /**
     * Determines whether the command sender is banned from using commands.
     *
     * @return true if the sender is blacklisted; false otherwise.
     */
    @Override
    public boolean isBlacklisted() {
        return bot.getCommandSystem().isBlacklisted(mask);
    }

    @Override
    public UserMask getMask() {
        return mask;
    }

    @Override
    public String getNickname() {
        return mask.getNickname();
    }

    @Override
    public String getUsername() {
        return mask.getUsername();
    }

    @Override
    public String getHostname() {
        return mask.getHostname();
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public void sendChat(String message) {
        connection.sendChat(getNickname(), message);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        return mask.equals(user.getMask()) &&
                connection.equals(user.getConnection());
    }

    @Override
    public int hashCode() {
        return mask.hashCode();
    }

    @Override
    public String toString() {
        return mask.toString();
    }
}
