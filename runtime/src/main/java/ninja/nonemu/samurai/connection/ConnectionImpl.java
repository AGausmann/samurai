package ninja.nonemu.samurai.connection;

import java.io.IOException;
import ninja.nonemu.irc.ChatRecipient;
import ninja.nonemu.irc.IrcUtil;
import ninja.nonemu.irc.Message;
import ninja.nonemu.samurai.BotImpl;
import org.apache.logging.log4j.Logger;

public class ConnectionImpl implements Connection {
    private final Logger logger;
    private final BotImpl bot;
    private final ninja.nonemu.irc.Connection connection;
    private final String id;
    private final String channels;
    private final boolean autoconnect;
    private final long cooldown;
    private final int limit;

    private long lastDisconnect;
    private int attempts;

    public ConnectionImpl(BotImpl bot, String id, ninja.nonemu.irc.Connection connection, String channels, boolean autoconnect, long cooldown, int limit) {
        this.bot = bot;
        this.id = id;
        this.connection = connection;
        logger = bot.getLogger();
        this.channels = channels;
        this.autoconnect = autoconnect;
        this.cooldown = cooldown;
        this.limit = limit;

        lastDisconnect = -1;
        attempts = 0;
    }

    public void run() {
        if (autoconnect && !isConnected()) {
            if (lastDisconnect == -1) {
                lastDisconnect = System.currentTimeMillis();
                attempts = 0;
            } else if (System.currentTimeMillis() - lastDisconnect > cooldown) {
                try {
                    connect();
                } catch (IOException e) {
                    attempts++;

                    if (limit > 0 && attempts >= limit) {
                        logger.error("Unable to connect to server. Removing from the bot.");
                        bot.getConnectionManager().removeConnection(this);
                    } else {
                        logger.error("Unable to connect to server. Attempting to reconnect when cooldown expires.", e);
                    }

                    return;
                }
            }
        }

        try {

            while (connection.canReadMessage()) {
                Message message = readMessage();

                if (message.getCommand().equalsIgnoreCase("PRIVMSG")) {
                    String sender = message.senderName();
                    String target = message.getArg(0);
                    String text = message.getArg(1);
                    bot.getEventSystem().dispatchEvent(new ChatEvent(getUser(sender),
                            recipientByName(target), text));
                }
                else if (message.getCommand().equalsIgnoreCase("NOTICE")) {
                    String sender = message.senderName();
                    String target = message.getArg(0);
                    String text = message.getArg(1);
                    bot.getEventSystem().dispatchEvent(new NoticeEvent(getUser(sender),
                            target.equals("*") ? recipientByName(getNickname()) : recipientByName(target), text));
                }
                else if (message.getCommand().equalsIgnoreCase("JOIN")) {
                    String user = message.senderName();
                    String channel = message.getArg(0);

                    if (user.equalsIgnoreCase(getNickname())) {
                        bot.getEventSystem().dispatchEvent(new BotJoinEvent(getChannel(channel)));
                    } else {
                        bot.getEventSystem().dispatchEvent(new UserJoinEvent(getUser(user),
                                getChannel(channel)));
                    }
                }
                else if (message.getCommand().equalsIgnoreCase("PART")) {
                    String user = message.senderName();
                    String channel = message.getArg(0);
                    String comment = message.getArgCount() > 1 ? message.getArg(1) : "No comment";

                    if (user.equalsIgnoreCase(getNickname())) {
                        bot.getEventSystem().dispatchEvent(new BotLeaveEvent(getChannel(channel), comment));
                    } else {
                        bot.getEventSystem().dispatchEvent(new UserLeaveEvent(getUser(user),
                                getChannel(channel), comment));
                    }
                }
                else if (message.getCommand().equalsIgnoreCase("KICK")) {
                    String sender = message.senderName();
                    String channel = message.getArg(0);
                    String user = message.getArg(1);
                    String comment = message.getArg(2);

                    if (user.equals(getNickname())) {
                        bot.getEventSystem().dispatchEvent(new BotKickEvent(getUser(sender), getChannel(channel),
                                comment));
                    } else {
                        bot.getEventSystem().dispatchEvent(new UserKickEvent(getUser(sender),
                                getUser(user), getChannel(channel), comment));
                    }
                }
                else if (message.getCommand().equalsIgnoreCase("QUIT")) {
                    String user = message.senderName();
                    String comment = message.getArgCount() > 0 ? message.getArg(0) : "No comment";
                    bot.getEventSystem().dispatchEvent(new UserQuitEvent(getUser(user), comment));
                }
                else if (message.getCommand().equalsIgnoreCase("KILL")) {
                    String sender = message.senderName();
                    String user = message.getArg(0);
                    String comment = message.getArgCount() > 1 ? message.getArg(1) : "No comment";

                    bot.getEventSystem().dispatchEvent(new UserKillEvent(getUser(sender),
                            getUser(user), comment));
                }
                else if (message.getCommand().equalsIgnoreCase("ERROR")) {
                    logger.info("Server error - {}", message.getArg(0));
                    if (isConnected()) {
                        disconnect();
                        break;
                    }
                }
                else if (message.getCommand().equals("001")) { // RPL_WELCOME
                    bot.getEventSystem().dispatchEvent(new BotConnectEvent(this));

                    if (channels != null && !channels.isEmpty()) {
                        connection.joinChannel(channels);
                    }
                }
                else if (message.getCommand().equals("432")) { // ERR_ERRONEUSNICKNAME
                    logger.error("Bad nickname (consider changing it): {}", message.getArg(1));
                }
                else if (message.getCommand().equals("433")) { // ERR_NICKNAMEINUSE
                    logger.warn("Nickname is in use; trying an alternate name");
                    setNickname(message.getArg(1) + "_");
                }
            }
        } catch (IOException e) {
            logger.error("Error reading messages from server", e);
        }
    }

    private Message readMessage() throws IOException {
        return connection.readMessage();
    }

    private ChatRecipient recipientByName(String name) {
        if (IrcUtil.isChannel(name)) {
            return getChannel(name);
        }
        return getUser(name);
    }

    public ninja.nonemu.irc.Connection getInnerConnection() {
        return connection;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void sendMessage(Message message) {
        connection.sendMessage(message);
    }

    @Override
    public boolean isConnected() {
        return connection.isConnected();
    }

    @Override
    public void connect() throws IOException {
        lastDisconnect = -1;
        connection.connect();
    }

    @Override
    public void disconnect(String comment) throws IOException {
        connection.disconnect(comment);
        bot.getEventSystem().dispatchEvent(new BotDisconnectEvent(this, comment));
    }

    @Override
    public void disconnect() throws IOException {
        disconnect("No comment");
    }

    @Override
    public void setNickname(String nickname) {
        connection.setNickname(nickname);
    }

    @Override
    public void joinChannel(String channel) {
        connection.joinChannel(channel);
    }

    @Override
    public void leaveChannel(String channel, String comment) {
        connection.leaveChannel(channel, comment);
    }

    @Override
    public void leaveChannel(String channel) {
        connection.leaveChannel(channel);
    }

    @Override
    public void sendChat(String target, String message) {
        connection.sendChat(target, message);
    }

    @Override
    public Channel getChannel(String channel) {
        return new ChannelImpl(this, channel);
    }

    @Override
    public User getUser(String userId) {
        return new UserImpl(bot, this, userId);
    }

    @Override
    public User getUser(UserMask userMask) {
        return new UserImpl(bot, this, userMask);
    }

    @Override
    public String getHost() {
        return connection.getHost();
    }

    @Override
    public int getPort() {
        return connection.getPort();
    }

    @Override
    public String getNickname() {
        return connection.getNickname();
    }
}
