package ninja.nonemu.samurai.connection;

public class ChannelImpl implements Channel {

    private final ConnectionImpl connection;
    private final String name;

    public ChannelImpl(ConnectionImpl connection, String name) {
        this.connection = connection;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public void sendChat(String message) {
        connection.sendChat(name, message);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Channel)) return false;

        Channel channel = (Channel) o;

        return connection.equals(channel.getConnection())
                && name.equalsIgnoreCase(channel.getName());
    }

    @Override
    public int hashCode() {
        int result = connection.hashCode();
        result = 31 * result + name.toLowerCase().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return name;
    }
}
