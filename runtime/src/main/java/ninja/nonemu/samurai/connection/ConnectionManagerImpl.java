package ninja.nonemu.samurai.connection;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import ninja.nonemu.samurai.BotImpl;
import org.apache.logging.log4j.Logger;

public class ConnectionManagerImpl implements ConnectionManager {
    private final BotImpl bot;
    private final Logger logger;
    private final Map<String, ConnectionImpl> connections;

    public ConnectionManagerImpl(BotImpl bot) {
        this.bot = bot;
        logger = bot.getLogger();
        connections = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
    }

    public void init() {
        logger.trace("Initializing connection manager");

        loadConnections();
    }

    public void run() {
        connections.values().forEach(ConnectionImpl::run);
    }

    public void cleanup() {
        logger.trace("Cleaning up connection manager");

        connections.values().forEach((connection) -> {
            if (connection.isConnected()) {
                try {
                    connection.disconnect();
                } catch (IOException e) {
                    logger.error("Unable to disconnect", e);
                }
            }
        });
        connections.clear();
    }

    private void loadConnections() {
        logger.trace("Loading connections");

        List<Properties> connectionProperties = new ArrayList<>();

        File connectionDir = new File("./connections");
        if (connectionDir.isDirectory()) {
            File[] files = connectionDir.listFiles((dir, name) -> name.endsWith(".properties"));

            if (files != null) {
                for (File file : files) {
                    Properties properties = new Properties();
                    try {
                        properties.load(new FileInputStream(file));
                    } catch (IOException e) {
                        logger.error("Cannot load connection properties", e);
                        continue;
                    }
                    connectionProperties.add(properties);
                }
            }
        } else if (connectionDir.exists()) {
            System.exit(1);
        } else {
            connectionDir.mkdir();
            Properties properties = new Properties();
            try {
                properties.load(getClass().getResourceAsStream("connection.properties"));
                properties.store(new FileOutputStream("./connections/default.properties"), "Example Connection");
            } catch (IOException e) {
                logger.error("Cannot write default connection configuration file", e);
                System.exit(1);
            }
            connectionProperties.add(properties);
        }

        for (Properties properties : connectionProperties) {
            addConnection()
                    .host(properties.getProperty("host"))
                    .port(Integer.parseInt(properties.getProperty("port")))
                    .nickname(properties.getProperty("nickname"))
                    .password(properties.getProperty("password"))
                    .channels(properties.getProperty("channels"))
                    .autoconnect(Boolean.parseBoolean(properties.getProperty("autoconnect")))
                    .autoconnectCooldown(Long.parseLong(properties.getProperty("autoconnect_cooldown")))
                    .autoconnectLimit(Integer.parseInt(properties.getProperty("autoconnect_limit")))
                    .build();
        }
    }

    @Override
    public ConnectionImpl[] getConnections() {
        ConnectionImpl[] result = new ConnectionImpl[connections.size()];
        connections.values().toArray(result);

        return result;
    }

    @Override
    public ConnectionImpl getConnection(String id) {
        return connections.get(id);
    }

    @Override
    public ConnectionBuilderImpl addConnection() {
        return new ConnectionBuilderImpl(bot, connection -> {
            if (connections.containsKey(connection.getId())) {
                throw new IllegalArgumentException("Connection with that ID has already been added.");
            }
            connections.put(connection.getId(), connection);
        });
    }

    @Override
    public void removeConnection(String id) {
        connections.remove(id);
    }
}
