package ninja.nonemu.samurai.connection;

import java.util.function.Consumer;
import ninja.nonemu.samurai.BotImpl;

public class ConnectionBuilderImpl implements ConnectionBuilder {
    private final BotImpl bot;
    private final Consumer<ConnectionImpl> buildCallback;

    private String id;
    private String host;
    private int port;
    private String nickname;
    private String channels;
    private String password;
    private boolean autoconnect;
    private long cooldown;
    private int limit;

    public ConnectionBuilderImpl(BotImpl bot, Consumer<ConnectionImpl> buildCallback) {
        this.bot = bot;
        this.buildCallback = buildCallback;

        host = null;
        port = 6667;
        nickname = "samurai";
        channels = null;
        password = null;
        autoconnect = true;
        cooldown = 5000;
        limit = 0;
    }

    @Override
    public ConnectionBuilderImpl id(String id) {
        this.id = id;
        return this;
    }

    @Override
    public ConnectionBuilderImpl host(String host) {
        this.host = host;
        return this;
    }

    @Override
    public ConnectionBuilderImpl port(int port) {
        this.port = port;
        return this;
    }

    @Override
    public ConnectionBuilderImpl nickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    @Override
    public ConnectionBuilderImpl channels(String channels) {
        this.channels = channels;
        return this;
    }

    @Override
    public ConnectionBuilderImpl channels(String[] channels) {
        return channels(String.join(",", (CharSequence[]) channels));
    }

    @Override
    public ConnectionBuilderImpl password(String password) {
        this.password = password;
        return this;
    }

    @Override
    public ConnectionBuilderImpl autoconnect(boolean autoconnect) {
        this.autoconnect = autoconnect;
        return this;
    }

    @Override
    public ConnectionBuilderImpl autoconnectCooldown(long cooldown) {
        this.cooldown = cooldown;
        return this;
    }

    @Override
    public ConnectionBuilderImpl autoconnectLimit(int limit) {
        this.limit = limit;
        return this;
    }

    @Override
    public Connection build() {
        if (id == null) {
            id = host;
        }
        if (host == null) {
            throw new IllegalArgumentException("Hostname cannot be null");
        }
        if (nickname == null) {
            throw new IllegalArgumentException("Nickname cannot be null");
        }

        ConnectionImpl connection = new ConnectionImpl(
                bot,
                id,
                new ninja.nonemu.irc.Connection(
                        host,
                        port,
                        nickname,
                        System.getProperty("user.name"),
                        "Samurai IRC Bot",
                        password
                ),
                channels,
                autoconnect,
                cooldown,
                limit
        );

        buildCallback.accept(connection);
        return connection;
    }
}
