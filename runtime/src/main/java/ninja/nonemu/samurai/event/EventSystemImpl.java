package ninja.nonemu.samurai.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import ninja.nonemu.samurai.BotImpl;
import org.apache.logging.log4j.Logger;

public class EventSystemImpl implements EventSystem {

    private final BotImpl bot;
    private final Logger logger;
    private final List<EventHandlerContainer> eventHandlers;
    private final Queue<Event> eventQueue;

    public EventSystemImpl(BotImpl bot) {
        this.bot = bot;
        logger = bot.getLogger();
        eventHandlers = new LinkedList<>();
        eventQueue = new LinkedList<>();
    }

    public void init() {
        logger.trace("Initializing event system");
    }

    public void run() {
        while (!eventQueue.isEmpty()) {
            Event event = eventQueue.remove();

            logger.debug("Dispatching event {}", event);

            for (EventHandlerContainer eventHandler : eventHandlers) {
                try {
                    eventHandler.handleEvent(event);
                } catch (InvocationTargetException e) {
                    logger.error("Error while handling event", e);
                }
            }
        }
    }

    public void cleanup() {
        logger.trace("Cleaning up event system");

        eventHandlers.clear();
        eventQueue.clear();
    }

    @Override
    public void registerHandlers(Object o) {
        logger.trace("Registering event handlers from " + o.toString());

        Class clazz = o.getClass();
        Method[] methods = clazz.getDeclaredMethods();

        for (Method method : methods) {
            logger.debug("Registering method {}", method);

            // If the method is not annotated correctly, skip without failure.
            if (!method.isAnnotationPresent(EventHandler.class)) {
                logger.debug("Method is not annotated; ignoring");
                continue;
            }

            // If the method does not have the proper signature (1 parameter extending Event), give an error message.
            if (method.getParameterCount() != 1 && !Event.class.isAssignableFrom(method.getParameterTypes()[0])) {
                logger.error("Method is annotated as an EventHandler but does not have the proper signature. " +
                        "EventHandler methods should take exactly one argument which is Event or a subclass.");
                continue;
            }

            eventHandlers.add(new EventHandlerContainer(o, method));
            Collections.sort(eventHandlers);
        }
    }

    @Override
    public void dispatchEvent(Event event) {
        logger.trace("Queuing event {}", event);

        eventQueue.add(event);
    }

    private class EventHandlerContainer implements Comparable<EventHandlerContainer> {
        Object instance;
        Method method;
        private final Class<?> targetClass;
        private final EventHandler annotation;

        private EventHandlerContainer(Object instance, Method method) {
            this.instance = instance;
            this.method = method;
            this.targetClass = method.getParameterTypes()[0];
            this.annotation = method.getAnnotation(EventHandler.class);
        }

        private void handleEvent(Event event) throws InvocationTargetException {
            if ((annotation.acceptSubclass() ? targetClass.isAssignableFrom(event.getClass())
                    : targetClass.equals(event.getClass())) && (annotation.acceptCancelled() || !event.isCancelled())) {
                try {
                    method.invoke(instance, event);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("Method has invalid access level; must be public", e);
                }
            }
        }

        @Override
        public int compareTo(EventHandlerContainer o) {
            return annotation.priority().compareTo(o.annotation.priority());
        }
    }
}
