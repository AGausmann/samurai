package ninja.nonemu.samurai.plugin;

import ninja.nonemu.samurai.BotImpl;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Map;
import java.util.TreeMap;
import java.util.jar.JarFile;

public class PluginManagerImpl implements PluginManager {

    private final BotImpl bot;
    private final Logger logger;

    private final Map<String, Plugin> plugins;

    public PluginManagerImpl(BotImpl bot) {
        this.bot = bot;
        logger = bot.getLogger();
        plugins = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
    }

    public void init() {
        logger.trace("Initializing plugin manager");

        logger.debug("Loading JAR plugins");
        File pluginDir = new File("./plugins");
        if (pluginDir.isDirectory()) {
            File[] files = pluginDir.listFiles((dir, name) -> name.toLowerCase().endsWith(".jar"));

            if (files != null) {
                for (File file : files) {
                    PluginBase plugin = null;
                    try {
                        JarFile jarFile = new JarFile(file);
                        String className = jarFile.getManifest().getMainAttributes().getValue("Plugin-Class");
                        logger.info("Loading plugin " + className);
                        URLClassLoader classLoader = new URLClassLoader(new URL[]{file.toURI().toURL()}, getClass().getClassLoader());
                        Class<?> pluginClass = classLoader.loadClass(className);

                        if (!PluginBase.class.isAssignableFrom(pluginClass)) {
                            logger.error("Plugin class does not extend PluginBase! Please report this error to developers.");
                            continue;
                        }

                        PluginBase.setDefaultBot(bot);
                        plugin = (PluginBase) pluginClass.getConstructor().newInstance();

                    } catch (IOException e) {
                        logger.error("I/O error while loading plugin. Check to see that your manifest file exists.", e);
                    } catch (ClassNotFoundException e) {
                        logger.error("Cannot find the specified plugin class", e);
                    } catch (NoSuchMethodException e) {
                        logger.error("Cannot load plugin. Make sure it has a constructor with no arguments.", e);
                    } catch (InstantiationException e) {
                        logger.error("Cannot load plugin. Make sure it it is not abstract and has a proper constructor.", e);
                    } catch (InvocationTargetException e) {
                        logger.error("Cannot load plugin. It threw an error during construction.", e);
                    } catch (IllegalAccessException e) {
                        logger.error("Cannot load plugin. Make sure the bot can access the plugin class.", e);
                    }

                    if (plugin != null) {
                        try {
                            registerPlugin(plugin);
                        } catch (Exception e) {
                            logger.error("Plugin threw an exception while initializing", e);
                        }
                    }
                }
            }
        } else if (pluginDir.exists()) {
            logger.error("Plugin directory `./plugins' is not a directory. Please move that file and restart the bot.");
            System.exit(1);
        } else {
            pluginDir.mkdir();
        }
    }

    public void run() {
        plugins.values().forEach((plugin) -> {
            try {
                plugin.run();
            } catch (Exception e) {
                logger.error("Plugin threw an exception while running", e);
            }
        });
    }

    public void cleanup() {
        logger.trace("Cleaning up plugin manager");

        String[] pluginNames = new String[plugins.size()];
        plugins.keySet().toArray(pluginNames);

        for (String pluginName : pluginNames) {
            try {
                unregisterPlugin(pluginName);
            } catch (Exception e) {
                logger.error("Plugin threw an exception while cleaning up", e);
            }
        }
    }

    @Override
    public void registerPlugin(Plugin plugin, String name) throws Exception {
        logger.info("Registering plugin {}", name);

        if (plugins.containsKey(name)) {
            logger.warn("A plugin is already registered as {}; aborting", name);
            return;
        }
        if (plugins.containsValue(plugin)) {
            logger.warn("That instance of Plugin ({}) is already registered; aborting", plugin);
            return;
        }

        plugins.put(name, plugin);
        try {
            plugin.init();
        } catch (Exception e) {
            plugins.remove(name);
            throw e;
        }
    }

    @Override
    public void unregisterPlugin(String name) throws Exception {
        logger.info("Unregistering plugin {}", name);

        if (!plugins.containsKey(name)) {
            logger.warn("No Plugin is registered as {}; aborting", name);
        }

        Plugin plugin = plugins.get(name);

        try {
            plugin.cleanup();
        } finally {
            plugins.remove(name);
        }
    }

    @Override
    public Plugin getPlugin(String name) {
        return plugins.get(name);
    }

    @Override
    public Map<String, Plugin> getPlugins() {
        Map<String, Plugin> result = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        result.putAll(plugins);
        return result;
    }
}
