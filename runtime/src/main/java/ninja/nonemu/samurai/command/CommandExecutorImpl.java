package ninja.nonemu.samurai.command;

import ninja.nonemu.samurai.BotImpl;
import ninja.nonemu.samurai.connection.Connection;
import ninja.nonemu.samurai.connection.User;
import ninja.nonemu.samurai.connection.UserMask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class CommandExecutorImpl {
    private final BotImpl bot;

    public CommandExecutorImpl(BotImpl bot) {
        this.bot = bot;

        CommandSystem commandSystem = bot.getCommandSystem();
        commandSystem.registerCommand(new CommandInfo(
                "version",
                "Provides information about the bot and its version.",
                "version",
                false
        ), this::handleVersion);
        commandSystem.registerCommand(new CommandInfo(
                "help",
                "Lists all commands available to the user.",
                "help <page|command>",
                false
        ), this::handleHelp);
        commandSystem.registerCommand(new CommandInfo(
                "ping",
                "Always returns a 'Pong!' Use it to check if the bot is online.",
                "ping",
                false
        ), this::handlePing);
        commandSystem.registerCommand(new CommandInfo(
                "oplist",
                "Manages the operator list.",
                "oplist add|remove <user> | enable|disable",
                true
        ), this::handleOplist);
        commandSystem.registerCommand(new CommandInfo(
                "whitelist",
                "Manages the user whitelist.",
                "whitelist add|remove <user> | enable|disable",
                true
        ), this::handleWhitelist);
        commandSystem.registerCommand(new CommandInfo(
                "blacklist",
                "Manages the user blacklist.",
                "blacklist add|remove <user> | enable|disable",
                true
        ), this::handleBlacklist);
        commandSystem.registerCommand(new CommandInfo(
                "quit",
                "Gracefully shuts the bot down",
                "quit",
                true
        ), this::handleQuit);
        commandSystem.registerCommand(new CommandInfo(
                "join",
                "Requests that the bot join a channel on the network.",
                "join <channel> [connection]",
                true
        ), this::handleJoin);
        commandSystem.registerCommand(new CommandInfo(
                "leave",
                "Requests that the bot leave a channel on the network.",
                "leave <channel> [connection]",
                true
        ), this::handleLeave);
        commandSystem.registerCommand(new CommandInfo(
                "msg",
                "Sends a message to a user or channel on the network.",
                "msg <target> <message>",
                true
        ), this::handleMsg);
        commandSystem.registerCommand(new CommandInfo(
                "plugins",
                "Returns a list of all registered plugins.",
                "plugins",
                false
        ), this::handlePlugins);
    }

    private boolean handleVersion(CommandSender sender, CommandInfo info, String label, String[] args) {
        sender.sendChat("Samurai v" + bot.getVersion() + ", a lightweight extensible IRC bot.");
        sender.sendChat("Written by Adam Gausmann (nonemu) <adam@nonemu.ninja>");
        sender.sendChat("Source code available at https://gitlab.com/AGausmann/samurai");
        return true;
    }

    private boolean handleHelp(CommandSender sender, CommandInfo info, String label, String[] args) {
        int page = 1;

        if (args.length > 0) {
            if (args[0].matches("[0-9|+]")) {
                page = Integer.parseInt(args[0]);
            } else {
                CommandInfo command = bot.getCommandSystem().getCommand(args[0]);

                if (command == null || (command.isPrivileged() && !sender.isOperator())) {
                    sender.sendChat(bot.getCommandSystem().getCommandPrefix() + args[0] + " is an unknown command.");
                } else {
                    sender.sendChat("Help for " + bot.getCommandSystem().getCommandPrefix() + args[0] + ":");
                    sender.sendChat("Description: " + command.getDescription());
                    sender.sendChat("Usage: " + bot.getCommandSystem().getCommandPrefix() + command.getUsage());
                }

                return true;
            }
        }

        CommandInfo[] commands = bot.getCommandSystem().getCommands();
        List<CommandInfo> commandList;

        if (sender.isOperator()) {
            commandList = Arrays.asList(commands);
        } else {
            commandList = new ArrayList<>(commands.length);
            for (CommandInfo command : commands) {
                if (!command.isPrivileged()) {
                    commandList.add(command);
                }
            }
        }

        int totalPages = (commandList.size() + 9) / 10;

        sender.sendChat("Help (Page " + page + " of " + totalPages + "):");
        for (int i = 10 * (page - 1); i < 10 * page && i < commandList.size(); i++) {
            sender.sendChat(commandList.get(i).getName() + ": " + commandList.get(i).getDescription());
        }

        return true;
    }

    private boolean handlePing(CommandSender sender, CommandInfo info, String label, String[] args) {
        sender.sendChat("Pong!");
        return true;
    }

    private boolean handleOplist(CommandSender sender, CommandInfo info, String label, String[] args) {
        if (args.length < 2) {
            return false;
        }

        if (args[0].equalsIgnoreCase("add")) {
            bot.getCommandSystem().addOperator(new UserMask(args[1]));
            sender.sendChat(args[1] + " has been added as an operator.");
            return true;
        }
        if (args[0].equalsIgnoreCase("remove")) {
            bot.getCommandSystem().removeOperator(new UserMask(args[1]));
            sender.sendChat(args[1] + " has been removed from operator status.");
            return true;
        }
        return false;
    }

    private boolean handleWhitelist(CommandSender sender, CommandInfo info, String label, String[] args) {
        if (args.length < 1) {
            return false;
        }

        if (args[0].equalsIgnoreCase("add")) {
            if (args.length < 2) {
                return false;
            }
            bot.getCommandSystem().addWhitelisted(new UserMask(args[1]));
            sender.sendChat(args[1] + " has been added to the whitelist.");
            return true;
        }
        if (args[0].equalsIgnoreCase("remove")) {
            if (args.length < 2) {
                return false;
            }
            bot.getCommandSystem().removeWhitelisted(new UserMask(args[1]));
            sender.sendChat(args[1] + " has been removed from the whitelist.");
            return true;
        }
        if (args[0].equalsIgnoreCase("enable")) {
            bot.getCommandSystem().setWhitelistEnabled(true);
            sender.sendChat("Whitelist has been enabled.");
            return true;
        }
        if (args[0].equalsIgnoreCase("disable")) {
            bot.getCommandSystem().setWhitelistEnabled(false);
            sender.sendChat("Whitelist has been disabled.");
            return true;
        }
        return false;
    }

    private boolean handleBlacklist(CommandSender sender, CommandInfo info, String label, String[] args) {
        if (args.length < 2) {
            return false;
        }

        if (args[0].equalsIgnoreCase("add")) {
            bot.getCommandSystem().addBlacklisted(new UserMask(args[1]));
            sender.sendChat(args[1] + " has been added to the blaclklist.");
            return true;
        }
        if (args[0].equalsIgnoreCase("remove")) {
            bot.getCommandSystem().removeBlacklisted(new UserMask(args[1]));
            sender.sendChat(args[1] + " has been removed from the blacklist.");
            return true;
        }
        return false;
    }

    private boolean handleQuit(CommandSender sender, CommandInfo info, String label, String[] args) {
        sender.sendChat("Bye!");
        bot.quit();
        return true;
    }

    private boolean handleJoin(CommandSender sender, CommandInfo info, String label, String[] args) {
        if (args.length > 0) {
            if (args.length == 1) {
                if (!(sender instanceof User)) {
                    sender.sendChat("Console must provide a connection name.");
                    return true;
                }
                sender.sendChat("Joining channel " + args[0]);
                ((User) sender).getConnection().joinChannel(args[0]);
            } else {
                sender.sendChat("Joining channel " + args[0] + " on " + args[1]);
                getConnection(args[1]).joinChannel(args[0]);
            }
            return true;
        }
        return false;
    }

    private boolean handleLeave(CommandSender sender, CommandInfo info, String label, String[] args) {
        if (args.length > 0) {
            if (args.length == 1) {
                if (!(sender instanceof User)) {
                    sender.sendChat("Console must provide a connection name.");
                    return true;
                }
                sender.sendChat("Leaving channel " + args[0]);
                ((User) sender).getConnection().leaveChannel(args[0]);
            } else {
                sender.sendChat("Leaving channel " + args[0] + " on " + args[1]);
                getConnection(args[1]).leaveChannel(args[0], "Requested by " + sender.getName());
            }
            return true;
        }
        return false;
    }

    private boolean handleMsg(CommandSender sender, CommandInfo info, String label, String[] args) {
        if (args.length > 1) {
            if (!(sender instanceof User)) {
                sender.sendChat("Console may not use this command.");
                return true;
            }

            String[] words = new String[args.length - 1];
            System.arraycopy(args, 1, words, 0, words.length);

            ((User) sender).getConnection().sendChat(args[0], String.join(" ", (CharSequence[]) words));

            return true;
        }
        return false;
    }

    private boolean handlePlugins(CommandSender sender, CommandInfo info, String label, String[] args) {
        Set<String> pluginSet = bot.getPluginManager().getPlugins().keySet();
        String[] pluginNames = new String[pluginSet.size()];
        pluginSet.toArray(pluginNames);
        sender.sendChat(String.format("Plugins (%d): %s", pluginNames.length, String.join(",", pluginNames)));

        return true;
    }

    private Connection getConnection(String name) {
        return bot.getConnectionManager().getConnection(name);
    }
}
