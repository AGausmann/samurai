package ninja.nonemu.samurai.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import ninja.nonemu.irc.Color;
import ninja.nonemu.samurai.BotImpl;
import ninja.nonemu.samurai.connection.ChatEvent;
import ninja.nonemu.samurai.connection.User;
import ninja.nonemu.samurai.connection.UserMask;
import ninja.nonemu.samurai.event.EventHandler;
import ninja.nonemu.samurai.event.Priority;
import org.apache.logging.log4j.Logger;

public class CommandSystemImpl implements CommandSystem {
    private final Logger logger;
    private final BotImpl bot;
    private final Set<UserMask> oplist;
    private final Set<UserMask> whitelist;
    private final Set<UserMask> blacklist;
    private final Map<String, CommandInfo> infoMap;
    private final Map<String, CommandExecutor> executorMap;
    private final Queue<Runnable> executionQueue;

    private String commandPrefix;

    public CommandSystemImpl(BotImpl bot) {
        logger = bot.getLogger();
        this.bot = bot;
        oplist = new TreeSet<>();
        whitelist = new TreeSet<>();
        blacklist = new TreeSet<>();
        infoMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        executorMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        executionQueue = new LinkedList<>();
    }

    public void init() {
        logger.trace("Initializing command system");

        commandPrefix = bot.getProperty("commandPrefix");

        clearCommands();
        clearAccessLists();
        loadAccessLists();

        bot.getEventSystem().registerHandlers(this);

        // Register the default commands.
        new CommandExecutorImpl(bot);
    }

    public void run() {
        while (!executionQueue.isEmpty()) {
            executionQueue.remove().run();
        }
    }

    public void cleanup() {
        logger.trace("Cleaning up command system");

        storeAccessLists();
        clearAccessLists();
        clearCommands();
    }

    private void loadAccessLists() {
        logger.debug("Loading access lists");

        File file = new File("oplist.txt");
        if (file.exists()) {
            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                while (reader.ready()) {
                    addOperator(new UserMask(reader.readLine()));
                }
            } catch (IOException e) {
                logger.error("Unable to load oplist.txt, consider fixing permissions", e);
            }
        } else {
            logger.warn("No operator list available, creating an empty one");
            try {
                file.createNewFile();
            } catch (IOException e) {
                logger.error("Unable to create oplist.txt", e);
            }
        }

        file = new File("whitelist.txt");
        if (file.exists()) {
            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                while (reader.ready()) {
                    addWhitelisted(new UserMask(reader.readLine()));
                }
            } catch (IOException e) {
                logger.error("Unable to load whitelist.txt, consider fixing permissions", e);
            }
        } else {
            logger.warn("No whitelist available, creating an empty one");
            try {
                file.createNewFile();
            } catch (IOException e) {
                logger.error("Unable to create whitelist.txt", e);
            }
        }

        file = new File("blacklist.txt");
        if (file.exists()) {
            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                while (reader.ready()) {
                    addBlacklisted(new UserMask(reader.readLine()));
                }
            } catch (IOException e) {
                logger.error("Unable to load blacklist.txt, consider fixing permissions", e);
            }
        } else {
            logger.warn("No blacklist available, creating an empty one");
            try {
                file.createNewFile();
            } catch (IOException e) {
                logger.error("Unable to create blacklist.txt", e);
            }
        }
    }

    private void storeAccessLists() {
        logger.debug("Storing access lists");

        File file = new File("oplist.txt");
        try (PrintWriter writer = new PrintWriter(new FileWriter(file))) {
            oplist.forEach(writer::println);
        } catch (IOException e) {
            logger.error("Unable to write oplist.txt", e);
        }

        file = new File("whitelist.txt");
        try (PrintWriter writer = new PrintWriter(new FileWriter(file))) {
            whitelist.forEach(writer::println);
        } catch (IOException e) {
            logger.error("Unable to write whitelist.txt", e);
        }

        file = new File("blacklist.txt");
        try (PrintWriter writer = new PrintWriter(new FileWriter(file))) {
            blacklist.forEach(writer::println);
        } catch (IOException e) {
            logger.error("Unable to write blacklist.txt", e);
        }
    }

    private void clearAccessLists() {
        logger.debug("Clearing access lists");

        oplist.clear();
        whitelist.clear();
        blacklist.clear();
    }

    private void clearCommands() {
        infoMap.clear();
        executorMap.clear();
        executionQueue.clear();
    }

    private void runCommand(CommandSender sender, String label, String[] args) {
        logger.info("User {} executed command {}", sender.getName(), label);

        if (!infoMap.containsKey(label) || !executorMap.containsKey(label)) {
            logger.info("Unknown command.");
            return;
        }

        CommandInfo info = infoMap.get(label);
        CommandExecutor executor = executorMap.get(label);

        if (info.isPrivileged() && !sender.isOperator()
                || isWhitelistEnabled() && !sender.isWhitelisted()
                || sender.isBlacklisted()) {
            logger.info("Access denied.");
            return;
        }

        try {
            boolean success = executor.onCommand(sender, info, label, args);

            if (!success) {
                sender.sendChat("Usage: " + commandPrefix + info.getUsage());
            }
        } catch (Exception e) {
            sender.sendChat("A fatal error occurred while running the command");
            logger.error("Error while running command", e);
        }
    }

    @EventHandler(priority = Priority.MONITOR, acceptSubclass = false)
    public void handleChatEvent(ChatEvent event) {
        String text = Color.stripColors(event.getText());
        if (!text.startsWith(commandPrefix)) {
            return;
        }

        if (!(event.getSender() instanceof User)) {
            return;
        }

        String[] words = text.split("\\s");
        String label = words[0].substring(commandPrefix.length());
        String[] args = new String[words.length - 1];
        System.arraycopy(words, 1, args, 0, args.length);

        CommandSender sender = (User) event.getSender();
        executeCommand(sender, label, args);
    }

    @Override
    public String getCommandPrefix() {
        return commandPrefix;
    }

    @Override
    public void registerCommand(CommandInfo info, CommandExecutor executor) {
        infoMap.put(info.getName(), info);
        executorMap.put(info.getName(), executor);
    }

    @Override
    public CommandInfo[] getCommands() {
        CommandInfo[] results = new CommandInfo[infoMap.size()];
        infoMap.values().toArray(results);

        return results;
    }

    @Override
    public CommandInfo getCommand(String name) {
        return infoMap.get(name);
    }

    @Override
    public void executeCommand(CommandSender sender, String label, String[] args) {
        executionQueue.add(() -> runCommand(sender, label, args));
    }

    @Override
    public boolean isOperator(UserMask mask) {
        for (UserMask userMask : oplist) {
            if (userMask.matches(mask)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void addOperator(UserMask mask) {
        oplist.add(mask);
    }

    @Override
    public void removeOperator(UserMask mask) {
        oplist.remove(mask);
    }

    @Override
    public boolean isWhitelisted(UserMask mask) {
        for (UserMask userMask : whitelist) {
            if (userMask.matches(mask)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void addWhitelisted(UserMask mask) {
        whitelist.add(mask);
    }

    @Override
    public void removeWhitelisted(UserMask mask) {
        whitelist.remove(mask);
    }

    @Override
    public boolean isWhitelistEnabled() {
        return Boolean.parseBoolean(bot.getProperty("enableWhitelist"));
    }

    @Override
    public void setWhitelistEnabled(boolean enabled) {
        bot.setProperty("enableWhitelist", Boolean.toString(enabled));
    }

    @Override
    public boolean isBlacklisted(UserMask mask) {
        for (UserMask userMask : blacklist) {
            if (userMask.matches(mask)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void addBlacklisted(UserMask mask) {
        blacklist.add(mask);
    }

    @Override
    public void removeBlacklisted(UserMask mask) {
        blacklist.remove(mask);
    }
}
