package ninja.nonemu.samurai.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import ninja.nonemu.samurai.Bot;
import ninja.nonemu.samurai.command.CommandSender;
import ninja.nonemu.samurai.connection.BotConnectEvent;
import ninja.nonemu.samurai.connection.BotDisconnectEvent;
import ninja.nonemu.samurai.connection.BotJoinEvent;
import ninja.nonemu.samurai.connection.BotKickEvent;
import ninja.nonemu.samurai.connection.BotLeaveEvent;
import ninja.nonemu.samurai.connection.ChatEvent;
import ninja.nonemu.samurai.connection.Connection;
import ninja.nonemu.samurai.connection.NoticeEvent;
import ninja.nonemu.samurai.connection.UserJoinEvent;
import ninja.nonemu.samurai.connection.UserKickEvent;
import ninja.nonemu.samurai.connection.UserKillEvent;
import ninja.nonemu.samurai.connection.UserLeaveEvent;
import ninja.nonemu.samurai.connection.UserQuitEvent;
import ninja.nonemu.samurai.event.EventHandler;
import ninja.nonemu.samurai.event.Priority;
import org.apache.logging.log4j.Logger;

public class ConsoleManager {
    private final Bot bot;
    private final Logger logger;

    private final BufferedReader consoleReader;
    private final ConsoleCommandSender consoleSender;

    public ConsoleManager(Bot bot) {
        this.bot = bot;
        logger = bot.getLogger();
        consoleReader = new BufferedReader(new InputStreamReader(System.in));
        consoleSender = new ConsoleCommandSender();
    }

    public void init() {
        logger.trace("Initializing console manager");

        bot.getEventSystem().registerHandlers(this);
    }

    public void run() {
        try {
            while (consoleReader.ready()) {
                String line = consoleReader.readLine();
                String[] words = line.split("\\s");
                if (words.length < 1) {
                    continue;
                }
                String label = words[0];
                String[] args = new String[words.length - 1];
                System.arraycopy(words, 1, args, 0, args.length);

                bot.getCommandSystem().executeCommand(consoleSender, label, args);
            }
        } catch (IOException e) {
            logger.info("Error while processing console input");
        }
    }

    public void cleanup() {
        logger.trace("Cleaning up console manager");
    }

    @EventHandler(priority = Priority.MONITOR)
    public void onBotConnectEvent(BotConnectEvent event) {
        Connection connection = event.getConnection();
        logger.info("Connected to {}:{} as {}", connection.getHost(), connection.getPort(), connection.getNickname());
    }

    @EventHandler(priority = Priority.MONITOR)
    public void onBotDisconnectEvent(BotDisconnectEvent event) {
        Connection connection = event.getConnection();
        logger.info("Disconnected from {}:{}", connection.getHost(), connection.getPort());
    }

    @EventHandler(priority = Priority.MONITOR)
    public void onBotJoinEvent(BotJoinEvent event) {
        logger.info("Joined {}", event.getChannel());
    }

    @EventHandler(priority = Priority.MONITOR, acceptSubclass = false)
    public void onBotLeaveEvent(BotLeaveEvent event) {
        logger.info("Left {} ({})", event.getChannel(), event.getComment());
    }

    @EventHandler(priority = Priority.MONITOR)
    public void onBotKickEvent(BotKickEvent event) {
        logger.info("Kicked from {} by {} ({})", event.getChannel(), event.getSender(), event.getComment()   );
    }

    @EventHandler(priority = Priority.MONITOR, acceptSubclass = false)
    public void onChatEvent(ChatEvent event) {
        logger.info("[{} -> {}] {}", event.getSender().getName(), event.getTarget().getName(), event.getText());
    }

    @EventHandler(priority = Priority.MONITOR, acceptSubclass = false)
    public void onNoticeEvent(NoticeEvent event) {
        logger.info("! [{} -> {}] {}", event.getSender().getName(), event.getTarget().getName(), event.getText());
    }

    @EventHandler(priority = Priority.MONITOR)
    public void onUserJoinEvent(UserJoinEvent event) {
        logger.info("{} has joined {}", event.getUser().getName(), event.getChannel().getName());
    }

    @EventHandler(priority = Priority.MONITOR, acceptSubclass = false)
    public void onUserLeaveEvent(UserLeaveEvent event) {
        logger.info("{} has left {} ({})", event.getUser().getName(), event.getChannel().getName(), event.getComment());
    }

    @EventHandler(priority = Priority.MONITOR)
    public void onUserKickEvent(UserKickEvent event) {
        logger.info("{} has been kicked from {} by {} ({})", event.getUser().getName(), event.getChannel().getName(),
                event.getSender().getName(), event.getComment());
    }

    @EventHandler(priority = Priority.MONITOR, acceptSubclass = false)
    public void onUserQuitEvent(UserQuitEvent event) {
        logger.info("{} has quit IRC ({})", event.getUser().getName(), event.getComment());
    }

    @EventHandler(priority = Priority.MONITOR)
    public void onUserKillEvent(UserKillEvent event) {
        logger.info("{} has been killed by {} ({})", event.getUser().getName(), event.getSender().getName(),
                event.getComment());
    }

    private class ConsoleCommandSender implements CommandSender {
        /**
         * Determines whether the command sender is allowed to run privileged commands.
         *
         * @return true if the sender is an operator; false otherwise.
         */
        @Override
        public boolean isOperator() {
            return true;
        }

        /**
         * Determines whether the command sender is allowed to run commands when the bot's whitelist is enabled.
         *
         * @return true if the sender is whitelisted; false otherwise.
         */
        @Override
        public boolean isWhitelisted() {
            return true;
        }

        /**
         * Determines whether the command sender is banned from using commands.
         *
         * @return true if the sender is blacklisted; false otherwise.
         */
        @Override
        public boolean isBlacklisted() {
            return false;
        }

        @Override
        public String getName() {
            return "CONSOLE";
        }

        @Override
        public void sendChat(String message) {
            logger.info(message);
        }
    }
}

